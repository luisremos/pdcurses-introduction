#include "interface.h"

bitword bits = 0x0000;      // displayed 16-bit binary word
int selectedBit = -1;       // index of selected bit (0-15)

void onSetSelected() {
	bits |=1 << selectedBit;
}

void onClearSelected() {
	bits &= ~(1 << selectedBit);
}

void onFlipSelected() {
	bits ^= (1 << selectedBit);
}

void onSetOther() {
	bits |= ~(1 << selectedBit);
}

void onClearOther() {
	bits &= (1 << selectedBit);
}

void onFlipOther() {
    bits ^= ~(1 << selectedBit);
}

void onSetAll() {
	bits = bits ^ bits;
	bits =~bits;
}

void onClearAll() {
	bits = bits ^ bits;
    // TODO: Implement this function
    // reset all bits to 0
}

void onFlipAll() {
	bits = ~bits;
    // TODO: Implement this function
    // negate all bits
}



#include "interface.h"

bitword input = 0x00;       // 16-bit input binary word
bitword mask = 0x00;        // 16-bit mask binary word
bitword output = 0x00;      // 16-bit output binary word

void onSetMasked() {
	output = input | mask;
}

void onClearMasked() {
	output = input & ~mask;
}

void onFlipMasked() {
	output = input ^ mask;
}

void onGetMasked() {
	output = input & mask;
}

void onSetUnmasked() {
	output = input | ~mask;
}

void onClearUnmasked() {
	output = input & mask;
}

void onFlipUnmasked() {
  	output = input ^ ~mask;
}

void onGetUnmasked() {
	output = input & ~mask;
}
#include "interface.h"

enum { BITS_COUNT = 16 };

typedef unsigned char bit;

// Arrays of bit elements (each array element must contain either 0 or 1)
bit valueBits[BITS_COUNT];
bit deltaBits[BITS_COUNT];

void onIncrement() {
	int b;
	for (b= 0; b < BITS_COUNT; b++) {
		valueBits[b] = 1 - valueBits[b];
		if (valueBits[b]) {
			return;
		}
	}
}

void onDecrement() {
	int b;
	for (b= 0; b < BITS_COUNT; b++) {
		valueBits[b] = 1 - valueBits[b];
		if (!valueBits[b]) {
			return;
		}
	}
}

unsigned int getUnsignedValue() {
	unsigned int sum = 0;
	int b;
	for (b = 0; b < BITS_COUNT; b++) {
		sum += valueBits[b] * (1 << b);
	}
	return sum;
}

signed int getSignedValue() {
	signed int result = (signed int)getUnsignedValue();
	if (!valueBits[BITS_COUNT-1]) {
		return result;
	}
	signed int complement = (1 << BITS_COUNT) - result;
	return -complement;
}
void onAddDelta() {
	int b;
	bit carry = 0;
	for (b = 0; b < BITS_COUNT; b++) {
		bit firstSum = valueBits[b] ^ deltaBits[b];
		bit firstCurry = valueBits[b] & deltaBits[b];
		bit secondSum = firstSum ^ carry;
		bit secondCarry = firstSum & carry;
		valueBits[b] = secondSum;
		carry = firstCurry | secondCarry;
	}
}

void onSubDelta() {
	int b;
	bit borrow = 0;
	for (b =0; b < BITS_COUNT; b++) {
		bit firstDiff = valueBits[b] ^ deltaBits[b];
		bit firstBorrow = ~valueBits[b] & deltaBits[b];
		bit secondDiff = firstDiff ^ borrow;
		bit secondBorrow = ~firstDiff & borrow;
		valueBits [b] = secondDiff;
		borrow = firstBorrow | secondBorrow;
		
	}
}
